<?php

namespace TextMedia\EncodingConverter\Tests;

use TextMedia\EncodingConverter\Converter;
use TextMedia\EncodingConverter\Detector;

/**
 * Тесты
 */
class ConverterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param string $source  Исходный текст в кодировке UTF-8
     * @param string $encoded Этот же текст в другой кодировке
     * @param string $charset Используемая кодировка
     *
     * @dataProvider textProvider
     *
     */
    public function testConverter(string $source, string $encoded, string $charset)
    {
        // проверим правильность определения кодировки
        $detected = Detector::detectByBom($encoded) ?: Detector::detectBySpecters($encoded);
        $this->assertEquals($charset, $detected);

        // проверим правильность декодирования текста
        $decoded = Converter::encode($encoded);
        $this->assertEquals($source, $decoded);
    }

    /**
     * @return array
     */
    public function textProvider(): array
    {
        // проверяемая исходная строка
        $source = 'Сорок тысяч обезьян в жопу сунули банан';

        // проверяемые кодировки
        $charsets = [
            'UTF-8', 'ISO8859-5', 'KOI8-R', 'WINDOWS-1251',
            'UTF-32BE', 'UTF-32LE', 'UTF-16BE', 'UTF-16LE',
        ];

        $data = array_map(function ($charset) use ($source) {
            return [$source, Converter::encode($source, $charset, 'UTF-8'), $charset];
        }, $charsets);

        return $data;
    }
}
