<?php

namespace TextMedia\EncodingConverter;

/**
 * Определение кодировки текста
 */
class Detector
{
    /** @var array Маркеры кодировок */
    private static $boms = [];

    /**
     * Получение маркера кодировки/всех маркеров
     *
     * @param string $charset Кодировка
     *
     * @return string
     */
    public static function getBom(string $charset): string
    {
        self::initBoms();
        return array_key_exists($charset, self::$boms) ? self::$boms[$charset] : '';
    }

    /**
     * Определение кодировки всеми доступными методами
     *
     * @param string $text Текст
     *
     * @return string
     */
    public static function detect(string $text): string
    {
        return self::detectByBom($text) ?: self::detectBySpecters($text);
    }

    /**
     * Определение кодировки по маркерам
     *
     * @param string $text Текст
     *
     * @return string|NULL
     */
    public static function detectByBom(string $text)
    {
        foreach (self::initBoms() as $charset => $bom) {
            if (substr($text, 0, strlen($bom)) === $bom) {
                return $charset;
            }
        }
        return null;
    }

    /**
     * Определение кодировки по спектрам
     *
     * @param string $text Текст
     *
     * @return string
     */
    public static function detectBySpecters(string $text): string
    {
        /** @var array Спектры */
        static $specters = [];

        // простая проверка регуляркой
        if (preg_match('#[а-яА-ЯёЁ]#u', $text)) {
            return 'UTF-8';
        }

        // инициализируем спектры
        if (empty($specters)) {
            foreach (glob(__DIR__ . '/specters/*.php') as $specter) {
                $charset = strtoupper(substr(basename($specter), 0, -4));
                $specters[$charset] = require($specter);
            }
        }

        // проверка при помощи спектров
        $weights = array_fill_keys(array_keys($specters), 0);
        for ($num = 0, $len = strlen($text) - 1; $num < $len; $num++) {
            $str = substr($text, $num, 2);
            foreach ($weights as $charset => &$weight) {
                if (!empty($specters[$charset][$str])) {
                    $weight += $specters[$charset][$str];
                }
            }
        }
        $summary = array_sum($weights);
        if (!empty($summary)) {
            foreach ($weights as &$weight) {
                $weight /= $summary;
            }
        }
        arsort($weights);
        $charsets = array_keys($weights);
        return reset($charsets);
    }

    /**
     * Определение кодировки по HTML/meta
     *
     * @param string $html HTML
     *
     * @return string|NULL
     */
    public static function detectByHtmlMeta(string $html)
    {
        $meta = $charset = null;
        preg_match_all('#<meta\s.+?>#i', $html, $meta, PREG_PATTERN_ORDER);
        while ($tag = array_pop($meta[0])) {
            if (preg_match('#http-equiv=.?Content-Type#i', $tag)
                and preg_match('#charset=(\S+)?#i', $tag, $charset)
            ) {
                return strtoupper(preg_replace('#[^a-z\d\-]#i', '', array_pop($charset)));
            }
        }
        return null;
    }

    /**
     * Определение кодировки по объявлению XML
     *
     * @param string $xml XML
     *
     * @return string|NULL
     */
    public static function detectByXmlDeclare(string $xml)
    {
        return preg_match('#^<\?xml.*?\sencoding="(.+?)".*?\?>#', ltrim($xml), $match)
                ? strtoupper(array_pop($match))
                : null;
    }

    /**
     * Инициализация набора маркеров кодировок
     *
     * @return array
     */
    private static function initBoms(): array
    {
        if (empty(self::$boms)) {
            $doubleZero = chr(0x00) . chr(0x00);
            self::$boms['UTF-16BE'] = chr(0xFE) . chr(0xFF);
            self::$boms['UTF-16LE'] = chr(0xFF) . chr(0xFE);
            self::$boms['UTF-32BE'] = $doubleZero . self::$boms['UTF-16BE'];
            self::$boms['UTF-32LE'] = self::$boms['UTF-16LE'] . $doubleZero;
            uasort(self::$boms, function ($x, $y) {
                return strlen($y) <=> strlen($x);
            });
        }
        return self::$boms;
    }
}
