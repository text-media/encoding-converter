<?php

namespace TextMedia\EncodingConverter;

/**
 * Конвертирование текста из одной кодировки в другую
 */
class Converter
{
    /** Добавлять маркер кодировки */
    const CONVERT_ADD_BOM = 0b0001;

    /**
     * Конвертирование текста из одной кодировки в другую
     *
     * @param string  $text         Исходный текст
     * @param string  $destionation OPTIONAL Целевая кодировка (по умолчанию - UTF-8)
     * @param string  $source       OPTIONAL Исходная кодировка (если не указана - детектится)
     * @param integer $options      OPTIONAL Опции детектирования/преобразования (битовая маска)
     *
     * @return string
     */
    public static function encode(
        string $text,
        string $destionation = 'UTF-8',
        string $source = null,
        int    $options = self::CONVERT_ADD_BOM
    ): string {
        if (empty($source)) {
            $source = Detector::detectByBom($text);
            if (!empty($source)) {
                $text = substr($text, strlen(Detector::getBom($source)));
            } else {
                $source = Detector::detectBySpecters($text);
            }
        } else {
            $source = strtoupper($source);
        }

        if (true) { // финт, чтобы IDE не ругалась не переприсваивание
            $destionation = strtoupper($destionation);
        }

        if ($source !== $destionation) {
            $text = (($options & self::CONVERT_ADD_BOM) ? Detector::getBom($destionation) : '')
                  . iconv($source, "{$destionation}//IGNORE", $text);
        }

        return $text;
    }
}
