# Encoding Converter

[![Packagist](https://img.shields.io/packagist/v/text-media/encoding-converter.svg)](https://packagist.org/packages/text-media/encoding-converter)
[![Packagist](https://img.shields.io/packagist/l/text-media/encoding-converter.svg)](https://packagist.org/packages/text-media/encoding-converter)

Пакет определения кодировки текста и конвертирования.

## Установка

Добавить в composer.json репозиторий и зависимость, чтобы получилось:

```bash
composer require text-media/encoding-converter
```

## Использование

Пример использования:

```php
use \TextMedia\EncodingConverter\{Converter, Detector};

// определение кодировки
$charset = Detector::detect($text);

// конвертирование из кодировки A в B
$encoded = Converter::encode($text, $b, $a);

// автоопределение кодировки и конвертирование в C
$encoded = Converter::encode($text, $c);

// автоопределение кодировки и конвертирование в UTF-8
$encoded = Converter::encode($text);
```

## Тесты

В корне проекта выполните:

```bash
./vendor/bin/phpunit -c ./
```
